#  CRON Parser

## Usage
`cat input.txt | parser 16:10`

#### Input format

```
30 1 /bin/run_me_daily
45 * /bin/run_me_hourly
* * /bin/run_me_every_minute
* 19 /bin/run_me_sixty_times
```
#### Output format
```
1:30 tomorrow - /bin/run_me_daily
16:45 today - /bin/run_me_hourly
16:10 today - /bin/run_me_every_minute
19:00 today - /bin/run_me_sixty_times
```

## Dependencies
The project uses Swift Package Manage as a packet management system. Cron Parser depends on [Swift Argument Parser](https://github.com/apple/swift-argument-parser) 

## TODO
1. Unit tests
2. Improve the documentation
2. Support internationalization

Feel free to write you comments/objections to [Dmitry Roytman](mailto:d.e.roytman@gmail.com?subject=[Element] CRON parser challenge)
