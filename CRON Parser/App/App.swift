//
//  App.swift
//  Scheduler Wrapper
//
//  Created by Dmitry Roytman on 16.09.2021.
//

import Foundation

struct App {
  private let statementParser: StatementParserProtocol
  private let schedulerFormatter: SchedulerFormatterProtocol
  
  init(
    statementParser: StatementParserProtocol = StatementParser(),
    schedulerFormatter: SchedulerFormatterProtocol
  ) {
    self.statementParser = statementParser
    self.schedulerFormatter = schedulerFormatter
  }
}
