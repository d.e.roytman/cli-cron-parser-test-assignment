import Foundation
import ArgumentParser

/// The entity handles user's input, parses command line argument(-s) and validates them
final class CommandLineParser: ParsableCommand {
    
    static var configuration: CommandConfiguration { .schedulerParserConfiguration }
    
    /// Stores the time from user's input
    /// The expected format is `hh:mm`
    @Argument(help: .help)
    var currentTime: String
    
    func validate() throws {
        guard !currentTime.isEmpty else {
            throw ValidationError("The simulated time should have hh:mm format")
        }
        guard currentTime.contains(.colonCharacter) else {
            throw ValidationError("The simulated time should have hh:mm format")
        }
        let elements = currentTime.split(separator: .colonCharacter)
        let (hoursSubstring, minutesSubstring) = (elements[0], elements[1])
        try minutesSubstring.makeMinutes()
        try hoursSubstring.makeHours()
    }
    
    func run() throws {
        var input: [String] = []
        // is it not 100% percrents safe but for the sake of simplicity I decided to use while-let cycle
        while let statement = readLine() {
            input.append(statement)
        }
        
        // Unfortunatelly, ArgumentParser library supposes to use only default initializer, which means that there is no ellegant way to inject dependency without using Singleton or dependency injection libraries. So, again, only for the sake of simplicity, I decided to use Factory pattern to build SchedulingService in place.
        let schedulingServiceFactory = SchedulingServiceFactory()
        let schedulingService = schedulingServiceFactory.makeSchedulingService(with: currentTime)
        try schedulingService.process(input: input)
    }
}

extension ArgumentHelp {
    fileprivate static var help: ArgumentHelp { ArgumentHelp(stringLiteral: .help) }
}

extension String {
    fileprivate static var help: String { "The simulated current time" }
}
