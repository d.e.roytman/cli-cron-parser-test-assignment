import Foundation
import ArgumentParser

extension CommandConfiguration {
  static var schedulerParserConfiguration: CommandConfiguration {
    return CommandConfiguration(
      commandName: .commandName,
      abstract: .abstract,
      discussion: .discussion,
      version: .version
    )
  }
}

extension String {
  fileprivate static var commandName: String { "CRON Parser" }
  fileprivate static var abstract: String { "Insert a scheduler input" }
  fileprivate static var discussion: String {
    return """
The utility takes a set of tasks as an input. Each if them running at least daily, which are scheduled with a simplified cron. The utility output is when each of them will next run.

The scheduler config's example:

30 1 /bin/run_me_daily
45 * /bin/run_me_hourly
* * /bin/run_me_every_minute
* 19 /bin/run_me_sixty_times

The first field is the minutes past the hour, the second field is the hour of the day and the third is the command to run. For both cases * means that it should run for all values of that field. In the above example run_me_daily has been set to run at 1:30am every day and run_me_hourly at 45 minutes past the hour every hour. The fields are whitespace separated and each entry is on a separate line.

The example of output for given above input and the simulated 'current time' command-line argument 16:10 

1:30 tomorrow - /bin/run_me_daily
16:45 today - /bin/run_me_hourly
16:10 today - /bin/run_me_every_minute
19:00 today - /bin/run_me_sixty_times
"""
  }
  fileprivate static var version: String { "1.0" }
}
