import Foundation
/// Scheduler Configuration Model
/// Represents the possible set up for hours, minutes, and scheduler
struct SchedulerConfiguration {
    typealias Minutes = Int
    typealias Hours = Int
    
    enum MinutesSetup {
        case particular(Minutes)
        case everyMinute
    }
    
    enum HoursSetup {
        case particular(Hours)
        case everyHour
    }
    
    // There was no answer about should scheduler be validated or not. So, I decided to keep an ability to validate it somehow
    enum SchedulerKind {
        case daily
        case hourly
        case everyMinute
        case sixtyTimes
        case other(path: String)
    }
    
    var minutesSetup: MinutesSetup
    var hoursSetup: HoursSetup
    var schedulerKind: SchedulerKind
}
