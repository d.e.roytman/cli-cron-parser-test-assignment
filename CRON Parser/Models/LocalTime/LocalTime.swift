
/// Local time model 
struct LocalTime {
    let hours: Int
    let minutes: Int
}

extension LocalTime {
    /// Factory method
    /// Receives an input string of format ` hh:mm` . E.g. 16:10
    /// N.B. All validation is supposed to happen on previous levels and unexpected input there leads the termination of the program
    static func makeLocalTime(input: String) -> LocalTime {
        return LocalTime(hours: input.hours, minutes: input.minutes)
    }
}

extension String {
    private var splitted: [String.SubSequence] { return split(separator: .colonCharacter) }
    
    fileprivate var minutes: Int {
        guard let minutesString = splitted.last else {
            // Hereinafter I decided to use fatalError because the possible output was validated in CommandLineParser class.
            // For obvious reasons, `fatalError` is not accceptable in production and all the actions should be done as much safe as possible with handling all possible exceptions.
            fatalError("The simulated time should have hh:mm format")
        }
        guard let minutes = Int(minutesString) else {
            fatalError("The minutes are supposed to be a number")
        }
        return minutes
    }
    
    fileprivate var hours: Int {
        guard let hoursString = splitted.first else {
            fatalError("The simulated time should have hh:mm format")
        }
        guard let hours = Int(hoursString) else {
            fatalError("The minutes are supposed to be a number")
        }
        return hours
    }
}
