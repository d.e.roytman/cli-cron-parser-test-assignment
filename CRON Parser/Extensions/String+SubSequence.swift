import Foundation
import ArgumentParser

extension String.SubSequence {
  @discardableResult
  // in order to save time I didn't separate concerns and didn't create separate factory methods and methods for validation.
  func makeMinutes() throws -> Int {
    guard !isEmpty else {
      throw ValidationError("The simulated time should have hh:mm format")
    }
    return try String(self).makeMinutes()
  }
  @discardableResult
  func makeHours() throws -> Int {
    guard !isEmpty else {
      throw ValidationError("The simulated time should have hh:mm format")
    }
    return try String(self).makeHours()
  }
}
