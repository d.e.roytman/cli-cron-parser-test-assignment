import Foundation
import ArgumentParser

extension String {
  static var daily: String { "/bin/run_me_daily" }
  static var hourly: String { "/bin/run_me_hourly" }
  static var everyMinute: String { "/bin/run_me_every_minute" }
  static var sixtyTimes: String { "/bin/run_me_every_minute" }
}

extension String {
  func makeMinutes() throws -> Int {
    guard let minutes = Int(self) else {
      throw ValidationError("The minutes are supposed to be a number")
    }
    guard (0...59).contains(minutes) else {
      throw ValidationError("The minutes are supposed to be in interval between 0 and 59")
    }
    return minutes
  }
  
  func makeHours() throws -> Int {
    guard let hours = Int(self) else {
      throw ValidationError("The hours are supposed to be a number")
    }
    guard (0...23).contains(hours) else {
      throw ValidationError("The hours are supposed to be in interval between 0 and 59")
    }
    return hours
  }
}
