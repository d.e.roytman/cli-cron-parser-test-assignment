//
//  Character.swift
//  CRON Parser
//
//  Created by Dmitry Roytman on 16.09.2021.
//

import Foundation

extension Character {
  static var colonCharacter: Character { ":" }
}
