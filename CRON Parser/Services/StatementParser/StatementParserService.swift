import Foundation

/// Parses a single line (statement) from user's input and produces a scheduler configuration `SchedulerConfiguration`
final class StatementParserService: StatementParserServiceProtocol {
  func parse(statement: Statement) throws -> SchedulerConfiguration {
    let splitted = statement.split(separator: .whitespace)
    let minutes = try parseMinutes(splitted[0])
    let hours = try parseHours(splitted[1])
    let scheduler = try parseScheduler(splitted[2])
    return SchedulerConfiguration(minutesSetup: minutes, hoursSetup: hours, schedulerKind: scheduler)
  }
  
  private func parseHours(_ rawValue: String.SubSequence) throws -> SchedulerConfiguration.HoursSetup {
    guard !rawValue.isEmpty else { throw ParsingError("Hours are expected not to be empty") }
    switch rawValue {
    case .asterisk:
      return .everyHour
    default:
      // Here should be rethrowing Validation exception as Parsing exception but for the sake of simplicty this action was omitted
      let hours = try rawValue.makeHours()
      return .particular(hours)
    }
  }
  
  private func parseMinutes(_ rawValue: String.SubSequence) throws -> SchedulerConfiguration.MinutesSetup {
    guard !rawValue.isEmpty else { throw ParsingError("Minutes are expected not to be empty") }
    switch rawValue {
    case .asterisk:
      return .everyMinute
    default:
      // Here should be rethrowing Validation exception as Parsing exception but for the sake of simplicty this action was omitted
      let minutes = try rawValue.makeMinutes()
      return .particular(minutes)
    }
  }
  
  private func parseScheduler(_ rawValue: String.SubSequence) throws -> SchedulerConfiguration.SchedulerKind {
    guard !rawValue.isEmpty else { throw ParsingError("Scheduler is expected not to be empty") }
    
    guard let kind = SchedulerConfiguration.SchedulerKind(rawValue: rawValue) else {
      throw ParsingError("Unexpected scheduler type")
    }
    return kind
  }
}

extension Character {
  fileprivate static var whitespace: Character { " " }
}

extension String {

  fileprivate static var asterisk: String { "*" }
}

extension SchedulerConfiguration.SchedulerKind {
  init?(rawValue: String.SubSequence) {
    guard !rawValue.isEmpty else { return nil }
    switch rawValue {
    case .daily:
      self = .daily
      
    case .hourly:
      self = .hourly
      
    case .everyMinute:
      self = .everyMinute
      
    case .sixtyTimes:
      self = .sixtyTimes

    default:
      self = .other(path: String(rawValue))
    }
  }
}
