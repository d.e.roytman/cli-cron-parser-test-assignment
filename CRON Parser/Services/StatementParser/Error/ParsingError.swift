struct ParsingError: Error, CustomStringConvertible {
  private(set) var message: String
  
  init(_ message: String) {
    self.message = message
  }
  
  var description: String {
    message
  }
}
