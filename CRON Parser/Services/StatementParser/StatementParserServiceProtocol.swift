import Foundation

protocol StatementParserServiceProtocol: Codable {
  typealias Statement = String
  func parse(statement: Statement) throws -> SchedulerConfiguration 
}
