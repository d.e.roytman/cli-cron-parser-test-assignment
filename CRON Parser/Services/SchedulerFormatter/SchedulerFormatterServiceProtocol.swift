import Foundation

protocol SchedulerFormatterServiceProtocol: AnyObject {
  func format(from configuration: SchedulerConfiguration) -> String
}
