protocol SchedulerFormatterServiceFactoryProtocol: AnyObject {
    func makeSchedulerFormatterService(with currentTime: String) -> SchedulerFormatterServiceProtocol
}

/// `SchedulerFormatterServiceFactory` encapsulates process of making an instance which conforms to `SchedulerFormatterServiceFactoryProtocol`
final class SchedulerFormatterServiceFactory: SchedulerFormatterServiceFactoryProtocol {
    func makeSchedulerFormatterService(with currentTime: String) -> SchedulerFormatterServiceProtocol {
        let currentTime = LocalTime.makeLocalTime(input: currentTime)
        return SchedulerFormatterService(currentTime: currentTime)
    }
}
