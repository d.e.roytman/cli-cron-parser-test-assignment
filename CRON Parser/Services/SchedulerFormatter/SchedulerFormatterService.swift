import Foundation
/**
 `SchedulerFormatterService` produces a formatted `String` on the basis currentTime `LocalTime` and configuration `SchedulerConfiguration`
 */
final class SchedulerFormatterService: SchedulerFormatterServiceProtocol {
  
  private typealias MinutesSetup = SchedulerConfiguration.MinutesSetup
  private typealias HoursSetup = SchedulerConfiguration.HoursSetup

  private let currentTime: LocalTime
  
  init(currentTime: LocalTime) {
    self.currentTime = currentTime
  }

  func format(from configuration: SchedulerConfiguration) -> String {
    let (computedHours, computedMinutes) = makeDestinationTime(
      with: configuration.hoursSetup,
      and: configuration.minutesSetup
    )
    let destination = makeDestination(with: computedHours, and: computedMinutes)
    let path = makeSchedulerPath(from: configuration)
    let formattedString = makeFormattedString(
      with: computedHours,
      computedMinutes: computedMinutes,
      destination: destination,
      path: path
    )
    return formattedString
  }
  
  private func makeDestinationTime(with hours: HoursSetup, and minutes: MinutesSetup) -> (hours: Int, minutes: Int) {
      let computedHours: Int
      let computedMinutes: Int
      switch (hours, minutes) {
      case (.everyHour, .everyMinute):
        computedHours = currentTime.hours
        computedMinutes = currentTime.minutes
        
      case (.particular(let hours), .everyMinute):
        computedHours = hours
        let isScheduledToCurrentHour = hours == currentTime.hours
        computedMinutes = isScheduledToCurrentHour ? currentTime.minutes : .zero
        
      case (.everyHour, .particular(let minutes)):
        let isRequestedMinutesPrecedesCurrent = minutes < currentTime.minutes
        computedHours = isRequestedMinutesPrecedesCurrent ? currentTime.hours.nextHour : currentTime.hours
        computedMinutes = minutes
        
      case (.particular(let hours), .particular(let minutes)):
        computedHours = hours
        computedMinutes = minutes
      }
    return (hours: computedHours, minutes: computedMinutes)
  }
  
  private func makeDestination(with computedHours: Int, and computedMinutes: Int) -> String {
    let isComputedHoursPrecedesCurrent = computedHours < currentTime.hours
    let isComputedHoursEqualsCurrent = computedHours == currentTime.hours
    let isComputedMinutesPrecedesCurrent = computedMinutes < currentTime.minutes
    let isComputedTimePrecedesCurrent = isComputedHoursPrecedesCurrent || (isComputedHoursEqualsCurrent && isComputedMinutesPrecedesCurrent)
    return isComputedTimePrecedesCurrent ? .tomorrow : .today
  }
  
  private func makeSchedulerPath(from configuration: SchedulerConfiguration) -> String {
    return configuration.schedulerPath
  }
  
  private func makeFormattedString(
    with computedHours: Int,
    computedMinutes: Int,
    destination: String,
    path: String
  ) -> String {
    return "\(computedHours.formatted):\(computedMinutes.formatted) \(destination) \(path)"
  }
}

extension Int {
  var nextHour: Int {
    return (self + 1) % 24
  }
  
  var formatted: String { String(format: "%02d", self) }
}

extension String {
  fileprivate static var today: String { "today" }
  fileprivate static var tomorrow: String { "tomorrow" }
}

extension SchedulerConfiguration {
  fileprivate var schedulerPath: String {
    switch schedulerKind {
    case .daily:
      return .daily
    case .hourly:
      return .hourly
    case .everyMinute:
      return .everyMinute
    case .sixtyTimes:
      return .sixtyTimes
    case let .other(path):
      return path
    }
  }
}
