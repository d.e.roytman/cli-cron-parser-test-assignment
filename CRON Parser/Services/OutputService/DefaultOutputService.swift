/// `DefaultOutputService` encapsulates output (printing) of any incomming String
final class DefaultOutputService: OutputServiceProtocol {
  func output(data: String) {
    print(data)
  }
}
