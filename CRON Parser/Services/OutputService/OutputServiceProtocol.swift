protocol OutputServiceProtocol {
  func output(data: String)
}
