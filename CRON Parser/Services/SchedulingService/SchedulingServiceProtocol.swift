protocol SchedulingServiceProtocol: AnyObject {
  func process(input: [String]) throws
}
