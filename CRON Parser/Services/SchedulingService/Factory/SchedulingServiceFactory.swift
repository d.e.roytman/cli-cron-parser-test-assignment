/// `SchedulingServiceFactory` encapsulates proces of making an instance which conforms to `SchedulingServiceProtocol`
final class SchedulingServiceFactory {
    private let outputService: OutputServiceProtocol
    private let statementParser: StatementParserServiceProtocol
    private let schedulerFormatterFactory: SchedulerFormatterServiceFactoryProtocol
    
    init(
        outputService: OutputServiceProtocol = DefaultOutputService(),
        statementParser: StatementParserServiceProtocol = StatementParserService(),
        schedulerFormatterFactory: SchedulerFormatterServiceFactoryProtocol = SchedulerFormatterServiceFactory()
    ) {
        self.outputService = outputService
        self.statementParser = statementParser
        self.schedulerFormatterFactory = schedulerFormatterFactory
    }
    
    func makeSchedulingService(with currentTime: String) -> SchedulingServiceProtocol {
        let formatter = schedulerFormatterFactory.makeSchedulerFormatterService(with: currentTime)
        return SchedulingService(statementParser: statementParser, formatter: formatter, outputService: outputService)
    }
}
