import Foundation

/**
 `SchedulingService` is responsible for handling user's input strings and print out the result to the output
 This class is a mediator beetwen underlying services: parser, formatter and output services
*/
final class SchedulingService: SchedulingServiceProtocol {
  private let statementParser: StatementParserServiceProtocol
  private let formatter: SchedulerFormatterServiceProtocol
  private let outputService: OutputServiceProtocol
  
  init(
    statementParser: StatementParserServiceProtocol,
    formatter: SchedulerFormatterServiceProtocol,
    outputService: OutputServiceProtocol
  ) {
    self.statementParser = statementParser
    self.formatter = formatter
    self.outputService = outputService
  }
  
  func process(input: [String]) throws {
    let schedulerConfigurations = try input.map { try statementParser.parse(statement: $0) }
    let formattedOutput = schedulerConfigurations.map { formatter.format(from: $0) }
    formattedOutput.forEach(outputService.output) 
  }
  
}
